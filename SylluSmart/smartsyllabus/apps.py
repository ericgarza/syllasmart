from django.apps import AppConfig


class SmartsyllabusConfig(AppConfig):
    name = 'smartsyllabus'
